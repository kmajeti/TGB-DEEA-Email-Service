package com.tgb.email.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tgb.email.service.EmailService;
import com.tgb.email.vo.ActivateUserEmailRequest;
import com.tgb.email.vo.EmailRequest;
import com.tgb.email.vo.ResetPasswordEmailRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */


@RestController
@RequestMapping("/tbg-email")
public class EmailController{

    @Autowired
    private EmailService emailService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/hello1")
    public String getHello()
    {
        return "Hello World";
    }
  
    
    @PostMapping("/v1")
    public ResponseEntity<Object>sendEmail(@Valid @RequestBody EmailRequest pEmailReq)
    {
        logger.info("EmailController:sendEmail:pEmailReq = " + pEmailReq);
        emailService.sendEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    
    
    @PostMapping("/aves-activate-user/v1")
    public ResponseEntity<Object>sendActivateUserEmail(@Valid @RequestBody ActivateUserEmailRequest pEmailReq)
    {
        logger.info("EmailController:sendActivateUserEmail:pEmailReq =" + pEmailReq);
        emailService.sendActivateUserEmail(pEmailReq);        	
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
        

    @PostMapping("/aves-reset-password/v1")
    public ResponseEntity<Object>sendResetPasswordEmail(@Valid @RequestBody ResetPasswordEmailRequest pEmailReq)
    {
        logger.info("EmailController:sendActivateUserEmail:pEmailReq = " + pEmailReq);
        emailService.sendResetPasswordEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}