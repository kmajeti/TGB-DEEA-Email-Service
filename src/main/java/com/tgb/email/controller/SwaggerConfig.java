package com.tgb.email.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import io.swagger.annotations.Contact;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	private Contact contactInfo = new Contact("Kishore Majeti", "http/www.tbg.com/kishore", "MajetiK@deea.thebuffalogroup.com");
	private Set<String> formatSet = new HashSet<String>(Arrays.asList("application/json", "application/xml"));
	
	@SuppressWarnings("deprecation")
	private ApiInfo apiInfo = new ApiInfo("Email API Service",
									"Email Service Documenation",
									"1.0",
									"http://www.tbg.com/terms-of-service",
									//contactInfo,
									"Kishore Majeti",
									"TBG Email Service 1.0",
									"http://www.tbg.com/emailService/License");
		
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo)
				.produces(formatSet)
				.consumes(formatSet);
	}

}
