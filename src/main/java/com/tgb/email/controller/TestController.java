package com.tgb.email.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@RestController
public class TestController {

	@Value("greetName")
	private String name;
	public String getHello() {
		return "Welcome " + name; 
	}
	
}
