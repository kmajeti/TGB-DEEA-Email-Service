package com.tgb.email.vo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@ApiModel(description = "Activate User - Email Request Payload")
public class ActivateUserEmailRequest{

	@ApiModelProperty(notes="destination email address - Mandatory field")
	@NotBlank
	@Email(message = "toAddress shouild be a valid email")
    private String toAddress;

	@ApiModelProperty(notes="User Name to which this email is adressed - Mandatory field")	
	@NotBlank(message = "userName cannot be blank")
    private String userName;
	
	@ApiModelProperty(notes="UUID of the user -  Mandatory field")		
	@NotBlank(message = "uuid cannot be blank")
	private String uuid;

    public String toString(){
        return String.join(System.getProperty("line.separator"),
        "ToAddress = " + toAddress,
        "userName = " + userName,
        "uuid = " + uuid);
    }
    
    
    public ActivateUserEmailRequest(){

    }

    public String getToAddress()
    {
        return toAddress;
    }

    public void setToAddress(String pStr)
    {
        this.toAddress = pStr;
    }
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String pStr)
    {
        this.userName = pStr;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String pStr)
    {
        this.uuid = pStr;
    }
}