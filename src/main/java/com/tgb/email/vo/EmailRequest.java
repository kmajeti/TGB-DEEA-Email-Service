package com.tgb.email.vo;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */
@ApiModel(description = "Email Request Payload")
public class EmailRequest{
    
	@ApiModelProperty(notes="from email address - Mandatory field")	
    @NotBlank(message = "fromAddress cannot be blank")	
	@Email(message = "fromAddress shouild be a valid email")
    private String fromAddress;
	
	
	@ApiModelProperty(notes="Destination email addresses - Mandatory field")	
    @NotBlank(message = "toAddresses cannot be blank")		
    private String[] toAddresses;
	
	@ApiModelProperty(notes="Email Subject - Mandatory field")		
    @NotBlank(message = "Subject cannot be blank")
    @Size(min=2, message="Subject should have length greater than 2")
    private String subject;
	
	@ApiModelProperty(notes="Email Content - mandatory field")			
    @NotBlank(message = "Content cannot be blank")
    @Size(min=2)
    private String content;
	
	@ApiModelProperty(notes="Email Content type - default is plain text")				
    private String contentType;

    public String toString(){
        
        StringBuilder sb = new StringBuilder();
        sb.append(" fromAddress : " + fromAddress);
        sb.append(" toAddresses : " + toAddresses);
        sb.append(" subject : " + subject);
        sb.append(" content : " + content);
        sb.append(" contentType : " + contentType);
        return sb.toString();
    }

    public EmailRequest(){

    }

    public String getFromAddress()
    {
        return fromAddress;
    }

    public void setFromAddress(String pStr)
    {
        this.fromAddress = pStr;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String pStr)
    {
        this.subject = pStr;
    }


    
    public String getContent()
    {
        return content;
    }

    public void setContent(String pStr)
    {
        this.content = pStr;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String pStr)
    {
        this.contentType = pStr;
    }

	public String[] getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(String[] toAddresses) {
		this.toAddresses = toAddresses;
	}
    
    
}