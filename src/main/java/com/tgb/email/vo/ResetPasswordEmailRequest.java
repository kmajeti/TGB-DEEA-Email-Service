package com.tgb.email.vo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@ApiModel(description = "Reset Password - Email Request Payload")
public class ResetPasswordEmailRequest{

	@ApiModelProperty(notes="destination email address - Mandatory field")	
	@NotBlank
	@Email(message = "toAddress shouild be a valid email")
    private String toAddress;
	
	@ApiModelProperty(notes="UUID of the user -  Mandatory field")		
	@NotBlank(message = "uuid cannot be blank")
	private String uuid;
    
	@ApiModelProperty(notes="Token -  Mandatory field")		
	@NotBlank(message = "token cannot be blank")	
    private String token;
    
    
	

    public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToAddress()
    {
        return toAddress;
    }

    public void setToAddress(String pStr)
    {
        this.toAddress = pStr;
    }
    
    public String toString() {
    	String s = String.join(System.getProperty("line.separator"),
    			"toAddress : " + toAddress,
    			"uuid : " + uuid,
    			"token : " + token
    			);
    	return s;
    }
 
}