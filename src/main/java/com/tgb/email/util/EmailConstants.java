package com.tgb.email.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */



@Component
public class EmailConstants{

    @Value("${email.resetPassword.subject}")
    public String RESET_PASWD_SUBJECT;
    
    @Value("${email.resetPassword.from}")
    public String RESET_PASWD_FROM;

    @Value("${email.resetPassword.template}")
    public String RESET_PASWD_TEMPLATE;

    
    @Value("${email.activateUser.from}")
    public String ACTIVATE_USER_FROM;
    
    @Value("${email.activateUser.subject}")
    public String ACTIVATE_USER_SUBJECT;
    
    @Value("${email.activateUser.template}")
    public String ACTIVATE_USER_TEMPLATE;

}



