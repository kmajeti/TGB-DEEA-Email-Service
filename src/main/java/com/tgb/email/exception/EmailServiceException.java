package com.tgb.email.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class EmailServiceException extends RuntimeException{

	private static final long serialVersionUID = 198676L;
	
	public EmailServiceException() {
		super();
	}

	
	public EmailServiceException(String message) {
		super(message);
	}

	public EmailServiceException(String message, Throwable cause) {
		super(message, cause);
	}
		
}
