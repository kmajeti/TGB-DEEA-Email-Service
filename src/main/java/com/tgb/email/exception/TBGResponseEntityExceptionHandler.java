package com.tgb.email.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 
 * @author Kishore Majeti
 *
 */

@ControllerAdvice
@RestController
public class TBGResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(EmailServiceException.class)
	public final ResponseEntity<Object> handleEmailServiceException(EmailServiceException ex, WebRequest request){
		ExceptionResponse er = new ExceptionResponse(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(er, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(new Date(), "Input Request Validation Failed", ex.getBindingResult().toString());
		return new ResponseEntity<Object>(er, HttpStatus.BAD_REQUEST);
	}

}
