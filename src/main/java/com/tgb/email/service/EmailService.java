
package com.tgb.email.service;

import com.tgb.email.exception.EmailServiceException;
import com.tgb.email.vo.ActivateUserEmailRequest;
import com.tgb.email.vo.EmailRequest;
import com.tgb.email.vo.ResetPasswordEmailRequest;


/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

public interface EmailService {
    
    public void sendEmail(EmailRequest  pEmailReq) throws EmailServiceException;
    public void sendActivateUserEmail(ActivateUserEmailRequest  pEmailReq) throws EmailServiceException;
    public void sendResetPasswordEmail(ResetPasswordEmailRequest  pEmailReq) throws EmailServiceException;
    
}
