package com.tgb.email.service;


import java.io.StringWriter;
import java.util.Properties;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.tgb.email.exception.EmailServiceException;
import com.tgb.email.util.EmailConstants;
import com.tgb.email.vo.ActivateUserEmailRequest;
import com.tgb.email.vo.EmailRequest;
import com.tgb.email.vo.ResetPasswordEmailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@Component
public class EmailServiceImpl implements EmailService{

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private EmailConstants emailConstants;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    	private String errMsg = "Exception encountered trying to send email";

    
    public void sendResetPasswordEmail(ResetPasswordEmailRequest  pEmailReq)
    throws EmailServiceException
   {
        System.out.println("begin EmailServiceImpl:sendResetPasswordEmail()");
        logger.info("Begin EmailServiceImpl:sendResetPasswordEmail() for request" + pEmailReq);        

        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(emailConstants.RESET_PASWD_FROM);
            helper.setTo(pEmailReq.getToAddress());
            helper.setSubject(emailConstants.RESET_PASWD_SUBJECT);   
            helper.setText(getResetPasswordEmailContent(pEmailReq), true);
            emailSender.send(message);
            logger.info("End EmailServiceImpl:sendResetPasswordEmail() for request" + pEmailReq);
        }
        catch(Exception ex){
        	logger.error(ExceptionUtils.getStackTrace(ex));
        	throw new EmailServiceException(errMsg, ex);
        }
    }



    public void sendActivateUserEmail(ActivateUserEmailRequest  pEmailReq)
    throws EmailServiceException
    {

        logger.info("Begin EmailServiceImpl:sendActivateUserEmail() for request" + pEmailReq);        
        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(emailConstants.ACTIVATE_USER_FROM);
            helper.setTo(pEmailReq.getToAddress());
            helper.setSubject(emailConstants.ACTIVATE_USER_SUBJECT);   
            helper.setText(getActivateUserEmailContent(pEmailReq), true);
            emailSender.send(message);
            logger.info("End EmailServiceImpl:sendActivateUserEmail() for request" + pEmailReq);    
        }
        catch(Exception ex){
        	logger.error(ExceptionUtils.getStackTrace(ex));
        	throw new EmailServiceException(errMsg, ex);
        }
    }
 

    public void sendEmail(EmailRequest  pEmailReq)
    throws EmailServiceException
    {

        logger.info("Begin EmailServiceImpl:sendEmail() for request" + pEmailReq);        
        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(pEmailReq.getFromAddress());
            helper.setTo(pEmailReq.getToAddresses());
            helper.setSubject(pEmailReq.getSubject());

            String contentType = pEmailReq.getContentType();
            contentType = (contentType == null) ? "" : contentType.trim();
            if(contentType.contains("html"))
                helper.setText(pEmailReq.getContent(), true);
            else
                helper.setText(pEmailReq.getContent());
        
             emailSender.send(message);
             logger.info("End EmailServiceImpl:sendEmail() for request" + pEmailReq);        
        }
        catch(Exception ex){
        	logger.error(ExceptionUtils.getStackTrace(ex));
        	throw new EmailServiceException(errMsg, ex);
        }        
        return;
    }


    public int sendEmailSimple(EmailRequest  pEmailReq){

        System.out.println("begin EmailServiceImpl:sendEmail()");
        int outputCode = 0;

        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(pEmailReq.getFromAddress());
            message.setTo(pEmailReq.getToAddresses());
            message.setSubject(pEmailReq.getSubject());
            message.setText(pEmailReq.getContent());
            emailSender.send(message);
            System.out.println("end EmailServiceImpl:sendEmail()");    
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return outputCode;
    }

    private String getActivateUserEmailContent(ActivateUserEmailRequest  pEmailReq)
    throws Exception
    {
        VelocityEngine velEngine = getVelocityEngine();
        VelocityContext context = new VelocityContext();    
        context.put("userName", pEmailReq.getUserName());
        context.put("uuid", pEmailReq.getUuid());
        StringWriter stringWriter = new StringWriter();
        velEngine.mergeTemplate(emailConstants.ACTIVATE_USER_TEMPLATE, "UTF-8", context, stringWriter);
        String text = stringWriter.toString();
        logger.info("Text build from template : " + text);
        return text;
    }


    private String getResetPasswordEmailContent(ResetPasswordEmailRequest pEmailReq)
    throws Exception
    {
        VelocityEngine velEngine = getVelocityEngine();
        VelocityContext context = new VelocityContext();  
        context.put("uuid", pEmailReq.getUuid());
        context.put("token", pEmailReq.getToken());
        StringWriter stringWriter = new StringWriter();
        velEngine.mergeTemplate(emailConstants.RESET_PASWD_TEMPLATE, "UTF-8", context, stringWriter);
        String text = stringWriter.toString();
        logger.info("Text build from template : " + text);
        return text;

    }

    private VelocityEngine getVelocityEngine() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("input.encoding", "UTF-8");
        properties.setProperty("output.encoding", "UTF-8");
        properties.setProperty("resource.loader", "class");
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine velocityEngine = new VelocityEngine(properties);
        return velocityEngine;
    }

}
